# react-firebase-cf-python-seed

## Frontend

- Using `React 16.9` + `Typescript 3.5` for primary frontend development
- Data are collected via `React hooks` from `Google Firestore`.
- Unit tests are run by `Jest 24.8` and Google Firebase is mocked by `firebase-nightlight`
- Using shallow renderer in unit tests


## Backend

- Using `Google Firebase` hosting for CDN delivery of web-app
- Using `Google Firestore` as primary NoSQL data store.
- `Google Cloud Functions` are used to do cloud based operations
- GCP provides Python 3.7 environment for functions
- `pytest` is used to validate functions' unit tests


## Open issues

- shallow rendering doesn't play well with async firestore hooks