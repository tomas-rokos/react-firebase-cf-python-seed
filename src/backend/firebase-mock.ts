import * as firebaseNighlight from 'firebase-nightlight';

import { setApp } from './firebase';

export function initializeApp(stub: any) {
    const mock = new firebaseNighlight.Mock(stub);
    setApp(mock.initializeApp({}));
}
