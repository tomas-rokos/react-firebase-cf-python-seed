import * as firebase from 'firebase/app';
import 'firebase/firestore';

let app: firebase.app.App;
export {app};

export function setApp(newapp: firebase.app.App) {
    app = newapp;
}

export function initializeApp() {
    const firebaseConfig = {
        apiKey: 'AIzaSyApMsSCCzy81YB1OgBUUZLbTxT6EBPgCDY',
        authDomain: 'playground-a8df2.firebaseapp.com',
        databaseURL: 'https://playground-a8df2.firebaseio.com',
        projectId: 'playground-a8df2',
        storageBucket: 'playground-a8df2.appspot.com',
        messagingSenderId: '948022233563',
        appId: '1:948022233563:web:5f30d13c91f30f3e'
      };
      // Initialize Firebase
      app = firebase.initializeApp(firebaseConfig);
}
