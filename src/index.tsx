import * as React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route } from 'react-router';

import Index from './routes/index/index';
import { initializeApp } from './backend/firebase';

import 'antd/dist/antd.css';

initializeApp();

render(
    (
        <Router>
            <Route path='/' exact={true} component={Index} />
        </Router>
    )
, document.getElementById('root'));
