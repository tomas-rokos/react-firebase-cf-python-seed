import * as _ from 'lodash';
import * as React from 'react';
import { useCollectionData } from 'react-firebase-hooks/firestore';

import { app } from '~backend/firebase';

export default function Index() {
    const [values, loading, error] = useCollectionData(
        app.firestore().collection('hints'),
        {
            idField: '_id'
        }
    );
    const items = (values || []).map( (item) => {
        return (
            <div key={_.get(item, ['_id'])}>
                {_.get(item, ['hint'])}
            </div>
        );
    });
    return (
        <div>
            {items}
        </div>
    );
}

