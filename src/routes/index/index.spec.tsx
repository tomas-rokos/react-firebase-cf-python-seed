import * as React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';

import { initializeApp } from '~backend/firebase-mock';

import Index from './index';

const firebase_mock = {
    firestore: {
        content: {
            hints: {
                mock_a: {
                    data: {
                        hint: 'first'
                    }
                },
                mock_b: {
                    data: {
                        hint: 'second'
                    }
                }
            }
        }
    },
    identities: [
        {
            'credentials': '',
            'email': 'tomas.rokos@gmail.com',
            'password': 'pokus',
            'token': 'authtoken',
            'uid': 'authuid'
        }
    ]
};

test('routes/index/index', () => {
    initializeApp(firebase_mock);
    const renderer = createRenderer();
    renderer.render(<Index />);
    const result = renderer.getRenderOutput();
    expect(result.type).toBe('div');
    expect(result.props.children.length).toBe(0);
});
