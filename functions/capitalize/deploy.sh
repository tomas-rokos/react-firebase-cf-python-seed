#!/bin/bash

gcloud functions deploy capitalize --runtime python37 --trigger-event providers/cloud.firestore/eventTypes/document.write --trigger-resource "projects/playground-a8df2/databases/(default)/documents/hints/{hintId}"