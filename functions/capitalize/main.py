from google.cloud import firestore


# Converts strings added to /hints/{pushId}/original to uppercase
def capitalize(data, context):
    path_parts = context.resource.split('/documents/')[1].split('/')
    collection_path = path_parts[0]
    document_path = '/'.join(path_parts[1:])

    affected_doc = firestore.Client().collection(collection_path).document(document_path)

    cur_value = data["value"]["fields"]["hint"]["stringValue"]
    new_value = cur_value.upper()
    print(f'Capitalize value: {cur_value} --> {new_value}')

    affected_doc.set({
        u'hint': new_value
    })